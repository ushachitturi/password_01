from tkinter import *
from tkinter import ttk, messagebox
import string
import random
import pyperclip
import sqlite3

def nextPage():
    page1_frame.pack_forget()
    page2_frame.pack()

def backPage():
    page2_frame.pack_forget()
    page1_frame.pack()

def selectStrength(strength_val):
    for strength, button in strength_buttons.items():
        if strength == strength_val:
            button.config(relief=SUNKEN, background="#4CAF50", foreground="black")
        else:
            button.config(relief=FLAT, background="#f0f0f0", foreground="black")

def generatePassword():
    chars = string.ascii_letters + string.digits + string.punctuation

    password_length = pw_length.get()

    if password_length <= 0:
        messagebox.showerror("Error", "Password length should be greater than 0.")
        return

    password_strength = strength_val.get()

   
    generated_password_text.config(state="normal")
    generated_password_text.delete(1.0, END)

    while True:
        new_password = ''.join(random.choice(chars) for _ in range(password_length))
        if not password_exists(connection, new_password):
            break

    
    generated_password_text.insert(END, new_password)
    insert_password(connection, new_password, password_strength)
    generated_password_text.config(state="disabled")

    
    generated_password_text.yview_moveto(0.0)

def copyToClipboard():
    password = generated_password_text.get(1.0, END).strip()
    pyperclip.copy(password)
    messagebox.showinfo("Copied to Clipboard", "Password has been copied to clipboard.")

def connect_to_database():
    try:
        connection = sqlite3.connect('passwords.db')
        return connection
    except sqlite3.Error as err:
        messagebox.showerror("Database Error", f"Failed to connect to database: {err}")
        return None

def create_table(connection):
    try:
        cursor = connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS passwords (
                            id INTEGER PRIMARY KEY,
                            password TEXT NOT NULL,
                            strength TEXT NOT NULL,
                            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                          )''')
        connection.commit()
        cursor.close()
    except sqlite3.Error as err:
        messagebox.showerror("Database Error", f"Failed to create table: {err}")

def insert_password(connection, password, strength):
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO passwords (password, strength) VALUES (?, ?)", (password, strength))
        connection.commit()
        cursor.close()
    except sqlite3.Error as err:
        messagebox.showerror("Database Error", f"Failed to insert password: {err}")

def password_exists(connection, password):
    cursor = connection.cursor()
    cursor.execute("SELECT COUNT(*) FROM passwords WHERE password = ?", (password,))
    count = cursor.fetchone()[0]
    cursor.close()
    return count > 0

def main():
    global page1_frame, page2_frame, strength_buttons, strength_val, pw_length, generated_password_text, connection

   
    connection = connect_to_database()
    if connection:
       
        create_table(connection)

    gui = Tk()
    gui.title("Password Generator")
    gui.config(bg='#ECECEC')
    width = 500
    height = 500
    screen_width = gui.winfo_screenwidth()
    screen_height = gui.winfo_screenheight()
    x = (screen_width / 2) - (width / 2)
    y = (screen_height / 2) - (height / 2)
    gui.geometry("%dx%d+%d+%d" % (width, height, x, y))

    
    page1_frame = Frame(gui)
    page1_frame.config(bg='#ECECEC')

    Label(page1_frame, text="Select Password Strength:", font=('Segoe UI', 24), foreground="black", background="lightgrey").pack(pady=50)

    strength_val = StringVar()
    strength_buttons = {}
    strengths = ["Strong", "Medium", "Weak"]
    for idx, strength in enumerate(strengths):
        button = Radiobutton(page1_frame, text=strength, variable=strength_val, value=strength, font=('Segoe UI', 18), foreground="black", indicatoron=0, command=lambda strength=strength: selectStrength(strength), highlightthickness=0, width=10)
        button.pack(pady=10)
        strength_buttons[strength] = button

    next_button = Button(page1_frame, text="Next", font=('Segoe UI', 14), command=nextPage, bg="#CCCCFF", fg="black", highlightthickness=0)
    next_button.pack(pady=20)

   
    page2_frame = Frame(gui)
    page2_frame.config(bg='#ECECEC')

    Label(page2_frame, text="Enter Password Length:", font=('Segoe UI', 14), foreground="black").grid(row=0, column=0, pady=10)
    pw_length = IntVar()
    Entry(page2_frame, textvariable=pw_length, font=('Segoe UI', 12), width=10).grid(row=0, column=1, pady=10)

    Button(page2_frame, text="Generate Password", font=('Segoe UI', 14), command=generatePassword, bg="#CCCCFF", fg="black", highlightthickness=0).grid(row=1, column=0, columnspan=2, pady=10)

   
    generated_password_text = Text(page2_frame, font=('Segoe UI', 12), width=30, height=6)
    generated_password_text.grid(row=2, column=0, columnspan=2, padx=5, pady=5)

   
    scrollbar = Scrollbar(page2_frame, orient=VERTICAL, command=generated_password_text.yview)
    scrollbar.grid(row=2, column=2, sticky="ns")
    generated_password_text.config(yscrollcommand=scrollbar.set)

    Button(page2_frame, text="Copy to Clipboard", font=('Segoe UI', 14), command=copyToClipboard, bg="#CCCCFF", fg="black", highlightthickness=0).grid(row=3, column=0, columnspan=2, pady=10)
    Button(page2_frame, text="Back", font=('Segoe UI', 14), command=backPage, bg="#CCCCFF", fg="black", highlightthickness=0).grid(row=4, column=0, columnspan=2, pady=10)
   

   
    page1_frame.pack(expand=True, fill="both")
    page2_frame.pack_forget()
    
    gui.mainloop()

if __name__ == "__main__":
    main()